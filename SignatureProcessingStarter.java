


import java.util.Collection;

/*
 *------------------------------------------------------------------------------
 * PPJ  (INF WP PPJ    ( & TI P2))
 * ===============================
 * VCS: git@BitBucket.org:schaefers/Prg_PPJ_B-F_CRCparallelization_Ref[.git]
 * For further information see ReadMe.txt
 *                                                Michael Schaefers  2017/05/29
 *------------------------------------------------------------------------------
 */
public class SignatureProcessingStarter {
    
    public static void main( final String... unused ){
//    	final SignatureProcessor_I sp = new SignatureProcessor();
    	final SignatureProcessor_I sp = new SignatureProcessor_Ref();
        
        final long timeStarted = System.nanoTime();
        final Collection<Item_I> signatureList =
            sp.computeSignatures(
                pathToRelatedFiles,
                filter
            );
        final long timeFinished = System.nanoTime();
        
        System.out.printf( "1000000af 100400007 104c11db7 127673637 __file_size | file_name...\n" );
        System.out.printf( "-vvvvvvvv--vvvvvvvv--vvvvvvvv--vvvvvvvv-------------+----------~~~\n" );
        for( final Item_I elem : signatureList ){
            System.out.printf(
                " %08x  %08x  %08x  %08x%12d : %s\n",
                elem.getSignature( 0 ),
                elem.getSignature( 1 ),
                elem.getSignature( 2 ),
                elem.getSignature( 3 ),
                elem.getFileSize(),
                elem.getFileName()
            );
        }//for
        System.out.printf( "\n" );
        System.out.printf( "After %s:\n", Utility.nanoSecondBasedTimeToString( timeFinished - timeStarted ));
    }//method()
    
    
    
    
    
//    final static String pathToRelatedFiles = "C:\\Users\\abv898\\Videos";             // path to the related files
    final static String pathToRelatedFiles = "/home/andi/Bilder";
//    final static String filter = ".*\\.(J|j)(P|p)(E|e)?(G|g)$";                 // filter resp. regex e.g. ".*\\.(J|j)(P|p)(E|e)?(G|g)$"
    final static String filter = ".*";
    
}//class
