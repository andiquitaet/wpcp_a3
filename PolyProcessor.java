


import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.concurrent.CountDownLatch;


/*
 *------------------------------------------------------------------------------
 * PPJ  (INF WP PPJ    ( & TI P2))
 * ===============================
 * VCS: git@BitBucket.org:schaefers/Prg_PPJ_B-F_CRCparallelization_Ref[.git]
 * For further information see ReadMe.txt
 *                                                Michael Schaefers  2017/05/29
 *------------------------------------------------------------------------------
 */
public class PolyProcessor implements Runnable {
	
	private int signature;
	private int[] lut;
	private File file;
	private CountDownLatch latch;
	final byte[] readBuffer = new byte[ParallelProcessor.lutSizeFor16bit];                 // read buffer for file access
    
    public PolyProcessor(File file, int[] lut, CountDownLatch latch) {
    	this.file = file;
		this.lut = lut;
		this.latch = latch;
	}
    
    public int getSignature() {
//    	System.out.println("siggi: " + signature);
    	return signature;
    }
    
    public int computeSignature(){
        final long fileSize = file.length();
        long bytesStillToCompute = (7 + fileSize) & ~0b11L;
        bytesStillToCompute = ( (bytesStillToCompute/12) + ( (bytesStillToCompute%12)>0 ? 1 : 0 )) *12;
        //
        int rc;                                                                 // Return Code
        //
        int signature = 0;
        
        try(
            final FileInputStream fis  = new FileInputStream( file );
        ){
            if( 4 <= file.length() ){
                // NO "ugly" special case
                
                loop:
                while(true){
                    rc = fis.read( readBuffer, 0, ParallelProcessor.amount64Ki );
                    if( ParallelProcessor.amount64Ki > rc )  break loop;
                    
                    int bufferIndex=0;
                    while( ParallelProcessor.amount64Ki >  bufferIndex ){
                        int word16bit = (0xff & readBuffer[bufferIndex+1])<<8  |  (0xff & readBuffer[bufferIndex]);
                        
                            final int index16bitLUT = signature & 0xFFFF;   // 16bit look-up, hence 16 bits required
                            final int valueLUT = lut[ index16bitLUT ];
                            signature = (signature >>> 16) ^ valueLUT ^ (word16bit<<16);
                        
                        bufferIndex+=2;
                    }//while
                    bytesStillToCompute -= ParallelProcessor.amount64Ki;
                }//while
                
                if( -1 == rc )  rc=0;
                for( int index=rc; index<bytesStillToCompute; index++ )  readBuffer[index] = 0;
                
                int bufferIndex=0;
                while( bytesStillToCompute >  bufferIndex ){
                    int word16bit = (0xff & readBuffer[bufferIndex+1])<<8  |  (0xff & readBuffer[bufferIndex]);
                    
                        final int index16bitLUT = signature & 0xFFFF;   // 16bit look-up, hence 16 bits required
                        final int valueLUT = lut[ index16bitLUT ];
                        signature = (signature >>> 16) ^ valueLUT ^ (word16bit<<16);
                    
                    bufferIndex+=2;
                }//while
                bytesStillToCompute -= ParallelProcessor.amount64Ki;
                
            //##################################################################
            }else{
                //=> the very special case
                
                // handle the only block (of file)
                rc = fis.read( readBuffer, 0, ParallelProcessor.amount64Ki );
                
                int theCompleteData = 0;
                switch ( rc ){
                    case -1: // file is empty => signature is zero
                        theCompleteData = 0;
                        return signature;
                      //break;
                    
                    case 1: // file contains only 1 byte
                        theCompleteData =         0
                        |  (readBuffer[0] & 0xff);                              // ..__.._.._.._B0  <- Byte 0
                      break;
                    
                    case 2: // file contains only 2 bytes
                        theCompleteData =           0
                        |  (readBuffer[0] & 0xff)                               // ..__.._.._.._B0  <- Byte 0
                        | ((readBuffer[1] & 0xff) <<  8);                       // ..__.._.._B1_..  <- Byte 1
                     break;
                    
                    case 3: // file contains only 3 bytes
                        theCompleteData =           0
                        |  (readBuffer[0] & 0xff)                               // ..__.._.._.._B0  <- Byte 0
                        | ((readBuffer[1] & 0xff) <<  8)                        // ..__.._.._B1_..  <- Byte 1
                        | ((readBuffer[2] & 0xff) << 16);                       // ..__.._B2_.._..  <- Byte 2
                      break;
                    
                    case 4: // file contains 4 bytes
                        theCompleteData =           0
                        |  (readBuffer[0] & 0xff)                               // ..__.._.._.._B0  <- Byte 0
                        | ((readBuffer[1] & 0xff) <<  8)                        // ..__.._.._B1_..  <- Byte 1
                        | ((readBuffer[2] & 0xff) << 16)                        // ..__.._B2_.._..  <- Byte 2
                        | ((readBuffer[3] & 0xff) << 24);                       // ..__B3_.._.._..  <- Byte 3
                      break;
                    
                    default:
                        throw new IllegalStateException( "UUUPPS how could this happen - did not expect to end up here" );
                      //break;
                }//switch
                //=> special case "empty / zero length" already handled
                //   in case of 1-4 bytes => 12 bytes have to be computed
                
                int bufferIndex=0;
                while( 12 > bufferIndex ){
                        final int index16bitLUT = signature & 0xFFFF;   // 16bit look-up, hence 16 bits required
                        final int valueLUT = lut[ index16bitLUT ];
                        signature = (signature >>> 16) ^ valueLUT ^ ((0xffff & theCompleteData)<<16);
                    theCompleteData >>>= 16;
                    bufferIndex+=2;
                }//while
            }//if
        }catch( final FileNotFoundException ex ){
            // TODO: react wise and clever ;-)
            ex.printStackTrace();
        }catch( final IOException ex ){
            // TODO: react wise and clever ;-)
            ex.printStackTrace();
        }//try
        
        return signature;
    }//method()

	@Override
	public void run() {
		signature = computeSignature();
		latch.countDown();
	}
	
    
}
