
import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.regex.Pattern;

/*
 *------------------------------------------------------------------------------
 * PPJ  (INF WP PPJ    ( & TI P2))
 * ===============================
 * VCS: git@BitBucket.org:schaefers/Prg_PPJ_B-F_CRCparallelization_Ref[.git]
 * For further information see ReadMe.txt
 *                                                Michael Schaefers  2017/05/29
 *------------------------------------------------------------------------------
 */
public class SignatureProcessor implements SignatureProcessor_I {
    
    @Override
    public Collection<Item_I> computeSignatures(
        final String pathToRelatedFiles,
        final String filter
    ){
        final File theDirectory = new File( pathToRelatedFiles );
        
        // do selfchecks
        if( ( ! theDirectory.exists())  ||  (! theDirectory.isDirectory()) ){
            throw new IllegalArgumentException(
                String.format(
                    "INVALID path: %s\n",
                    pathToRelatedFiles
                )
            );
        }//if
        //\=> arguments are valid
        
        final Collection<Item_I> coll = new ArrayList<>();
        int cores = Runtime.getRuntime().availableProcessors();
        ExecutorService executor = Executors.newFixedThreadPool(cores);
        System.out.println("Available cores: " + cores);
        final ParallelProcessor darp = new ParallelProcessor(executor);
        computeSignatures( darp, coll, theDirectory, filter );
        executor.shutdown();
        return coll;
    }//method()
    //
    private void computeSignatures(
        final ParallelProcessor darp,
        final Collection<Item_I> coll,
        final File theCurrentDirectory,
        final String filter
    ){
        final File[] currentFiles = theCurrentDirectory.listFiles();
        if( null != currentFiles ){
            for ( final File file : theCurrentDirectory.listFiles() ){
                if( file.isDirectory() && file.canExecute() ){
                    computeSignatures( darp, coll, file, filter );
                }else{
                    if( hasToBeConsidered( file, filter )){
                        coll.add( new Item( file.getName(),  file.length(),  darp.computeSignatures( file )));
                    }//if
                }//if
            }//for
        }//if
    }//method()
    
    // check access rights - TODO resp. NOT working
    private boolean hasToBeConsidered(
        final File file,
        final String filter
    ){
        final String fileName = file.getName();
        return Pattern.matches( filter, fileName )
            /*
            && ( ! file.canRead() )
            && ( ! Files.isSymbolicLink( file.toPath() ))
            */
        ;
    }//method()
    
    
    
    public SignatureProcessor(){
    }//constructor()
        
}// class
