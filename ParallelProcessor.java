


import java.io.File;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import jdk.nashorn.internal.runtime.PrototypeObject;


/*
 *------------------------------------------------------------------------------
 * PPJ  (INF WP PPJ    ( & TI P2))
 * ===============================
 * VCS: git@BitBucket.org:schaefers/Prg_PPJ_B-F_CRCparallelization_Ref[.git]
 * For further information see ReadMe.txt
 *                                                Michael Schaefers  2017/05/29
 *------------------------------------------------------------------------------
 */
public class ParallelProcessor {
	
	private ExecutorService executor;
    
    public int[] computeSignatures( final File file ){
        final int[] signature = new int[numberOfPolynomials];
        
        CountDownLatch latch = new CountDownLatch(numberOfPolynomials);
        PolyProcessor[] polyProcs= new PolyProcessor[numberOfPolynomials];
        
        PolyProcessor tempHolder;
        for (int i = 0; i < numberOfPolynomials; i++) {
        	tempHolder = new PolyProcessor(file, lut[i], latch);
        	polyProcs[i] = tempHolder;
        	executor.execute(tempHolder);
        }
        
        try {
			latch.await();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
        
        for(int i = 0; i < numberOfPolynomials; i++){
        	signature[i] = polyProcs[i].getSignature();
        }
        
        return signature;
    }//method()
    
    
    
    
    
    // ATTENTION!
    // This constructor sets up object specific and class specific stuff
    public ParallelProcessor(ExecutorService executor){
    	this.executor = executor;
        // check arguments
        if( numberOfPolynomials != NAME_OF_POLYNOMIAL.values().length ){
            throw new IllegalStateException();
        }//if
        if( numberOfPolynomials != originalPolynomial.length ){
            throw new IllegalStateException();
        }//if
        for( int indx=originalPolynomial.length;  --indx>=0;  ){
            if( expectedPolynomialBitSize != Utility.numberOfBitsNeededForCoding( originalPolynomial[indx] )){
                throw new IllegalStateException();
            }//if
        }//for
        //=>constant definitions are NOT obviously inconsistent
        
        
        
        // construct object initial state
        for( int polyId=lut.length;  --polyId>=0;  ){ 
            final long poly = originalPolynomial[polyId];
            lut[polyId] = new int[amount64Ki];
            
            
            // working with original polynomial
            for( int indx=amount64Ki;  --indx>=0;  ){
                long signature = indx;
                // da mit dem Original-Polynom gearbeitet wird, muss erst verknuepft und dann geSHIFTet werden - quasi die "Original-Idee"
                for( int stillToDo=lookup16bit;  --stillToDo>=0;  ){            // 16 bit look-bit means 16 bits have to be computed for look-up
                    if( 0b1 == ( signature & 0b1 )){
                        signature ^= poly;
                    }//if
                    signature >>>= 1;
                }//for
                
                lut[polyId][indx] = (int)( signature );
                
                if( enSelfTest ){
                    final long toBigFor32Bit = 1L << 32;
                    if( signature >= toBigFor32Bit ){
                        throw new IllegalStateException( String.format( "--->>>ERR: invalid signature occurred : %d\n", signature ));
                    }//if
                }//if
            }//for
            
        }//for
        
        if( enSelfTest ) selfTestCheckLUTs();
    }//constructor()
    
    
    
    private void selfTestCheckLUTs(){
        // Wenn ALLES richtig gemacht wurde, dann darf es KEINE Doppelten in der (jeweiligen) LUT geben
        // (Notwendiges Kriterium aber NICHT hinreichend)
        
        for( int polyId=lut.length;  --polyId>=0;  ){ 
            
            // alternative1 for selfcheck
            // ============
            final int numberOfRequiredElements = (int)((1L << 32) >>> 6 );      // <- 67108864 =
            final long[] marker = new long[numberOfRequiredElements];
            for( int i=marker.length;  --i>=1;  )  marker[i] = 0;
            loop:
            for( final int elem : lut[polyId] ){
                int index  = elem >>> 6;
                int bitPos = elem & 0b0011_1111;
                long bit = 0b1L << bitPos;
              //if( bitPos<0 || 63<bitPos ){ System.out.printf( "\n\n--->>>ERR: %d -> %d<<<---\n\n",  bitPos, bit );  break loop; }
                if( bit == ( marker[index] & bit )){
                    System.out.printf(
                        "ERR: %d  ->  %d:%d  {%016x}\n",
                        elem,
                        index,
                        bitPos,
                        marker[index]
                    );
                    break loop;
                }//if
                marker[index] |= bit;
            }//for
            //
            // Der erste Eintrag (der Eintrag fuer 0) muss 0 sein
            if( 0 != lut[polyId][0] ){
                System.out.printf(
                    "ERR: LUT[0]=0 != %d\n",
                    lut[polyId][0]
                );
            }//if
            
            // alternative2 for selfcheck
            // ============
            final Set<Integer> markerSet = new HashSet<>();
            loop:
            for( final int elem : lut[polyId] ){
                if( ! markerSet.add( elem )){
                    System.out.printf(
                        "ERR: %d  occured twice\n",
                        elem
                    );
                    break loop;
                }//if
            }//for
        }//for
        
    }//method()
    
    
    
    
    
    final boolean enSelfTest =  true;
    
    // Vieles vom Folgenden wird NICHT gebraucht, aber als "Angebot" ist es mit aufgefuehrt.
    // Um ein einheitliches Naming sicherzustellen, bitte die entsprechenden Namen verwenden,
    // wenn auch die entsprechenden Dinge verwendet werden.
    static enum NAME_OF_POLYNOMIAL { A, B, C, D };
    final static long polynomialA = 0x1000000afL;                               // 32 bit polynomial with MSB on left side and on right side LSB
    final static long polynomialB = 0x100400007L;                               // 32 bit polynomial with MSB on left side and on right side LSB
    final static long polynomialC = 0x104c11db7L;                               // 32 bit polynomial with MSB on left side and on right side LSB
    final static long polynomialD = 0x127673637L;                               // 32 bit polynomial with MSB on left side and on right side LSB
    final static int A = NAME_OF_POLYNOMIAL.A.ordinal();
    final static int B = NAME_OF_POLYNOMIAL.B.ordinal();
    final static int C = NAME_OF_POLYNOMIAL.C.ordinal();
    final static int D = NAME_OF_POLYNOMIAL.D.ordinal();
    final static long[] originalPolynomial = {
        Utility.mirror( polynomialA ),                                          // the 32 bit polynomial "A" with LSB on left side and on right side MSB
        Utility.mirror( polynomialB ),                                          // the 32 bit polynomial "B" with LSB on left side and on right side MSB
        Utility.mirror( polynomialC ),                                          // the 32 bit polynomial "C" with LSB on left side and on right side MSB
        Utility.mirror( polynomialD )                                           // the 32 bit polynomial "D" with LSB on left side and on right side MSB
    };
    final static long[] adaptedPolynomial = {
        originalPolynomial[A] >>> 1,                                            // the adapted 32 bit polynomial "A" with LSB on left side and on right side MSB, the original MSB is already shifted out
        originalPolynomial[B] >>> 1,                                            // the adapted 32 bit polynomial "B" with LSB on left side and on right side MSB, the original MSB is already shifted out
        originalPolynomial[C] >>> 1,                                            // the adapted 32 bit polynomial "C" with LSB on left side and on right side MSB, the original MSB is already shifted out
        originalPolynomial[D] >>> 1                                             // the adapted 32 bit polynomial "D" with LSB on left side and on right side MSB, the original MSB is already shifted out
    };
    final static int numberOfPolynomials = originalPolynomial.length;
    final static int expectedPolynomialBitSize = 33;                            // 32, .., 0 resp. 0, .., 32
    //
    // 16 bit look-up table
    final static int lookup16bit = 16;                                          //
    final static int amount64Ki = 1 << 16;                                      // 2^16 = 65_536
    final static int lutSizeFor16bit = ((amount64Ki/12) + ((amount64Ki%12) > 0  ?  1  :  0 )) *12;
    //
    // 24 bit look-up table
    final static int lookup24bit = 24;                                          //
    final static int amount24Mi = 1 << 24;                                      // 2^24 = 16_777_216
    
    final static int[][] lut = new int[originalPolynomial.length][];            // Look-Up Table
    final static int[] lutA = lut[A];                                           // Look-Up Table for polynomial "A"
    final static int[] lutB = lut[B];                                           // Look-Up Table for polynomial "B"
    final static int[] lutC = lut[C];                                           // Look-Up Table for polynomial "C"
    final static int[] lutD = lut[D];                                           // Look-Up Table for polynomial "D"
    
    
    
//    final static byte[] readBuffer = new byte[lutSizeFor16bit];                 // read buffer for file access
    
}//class
